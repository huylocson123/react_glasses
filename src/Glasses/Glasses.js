import React, { Component } from "react";
import { listGlasses } from "./DataGlasses";
import GlassItem from "./GlassItem";
import "./style.css";

export default class Glasses extends Component {
  state = {
    listGlasses: listGlasses,
  };

  renderGlasses = () => {
    return this.state.listGlasses.map((item) => {
      return <GlassItem DataGlasses={item} key={item.id} />;
    });
  };

  render() {
    return (
      <div>
        <div className="header">
          <div className="title">
            <p>TRY APP GLASSES ONLINE</p>
          </div>
        </div>
        <div className="container">
          <div className="col-4 vglasses p-0 ">
            <div class="vglasses__card">
              <div class="vglasses__model" id="avatar">
                <div class="model-glass" id="avatarGlass"></div>
              </div>
              <div id="glassesInfo" class="vglasses__info"></div>
            </div>
          </div>
          <div className="glasses">{this.renderGlasses()}</div>
        </div>
      </div>
    );
  }
}
