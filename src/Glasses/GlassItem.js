import React, { Component } from "react";

export default class GlassItem extends Component {
  render() {
    let { url } = this.props.DataGlasses;
    return (
      <div className="item">
        <img src={url} alt="" className="col-4" />
      </div>
    );
  }
}
